import numpy as np
import sys
from scipy.spatial.distance import cityblock
import math
move = np.zeros((3,5))
move[0][1] = 1
move[1][0] = 1
move[2][1] = 1
move[1][4] = 2
player = input("Do you want tobe hare or hound ?")
depth = int(input('Choose Level of difficulty from 1 to 10 : '))
if player == "hare":
    player = "hare"
else:
    player = "hound" 

def print_invalid():
    print('Invalid Move !')
    sys.exit(0)

def print_game_state(move):
    illegal_moves = [(0, 0), (2, 0), (0, 4), (2, 4)]
    for i in range(move.shape[0]):
        buffer = ''
        for j in range(move.shape[1]):
            if move[i][j] == 1:
                buffer  += 'X\t'
            elif move[i][j] == 2:
                buffer += '0\t'
            elif (i,j) in illegal_moves:
                buffer += ' \t'
            else:
                buffer += '-\t'
        print(buffer)
    print('-' * 30)   
            
print("Initial state of game move")
print(print_game_state(move))
  
print("coordinates of game move-------")
print("coordinates of first row:         (0, 1) (0, 2) (0, 3)")
print("coordinates of second row: (1, 0) (1, 1) (1, 2) (1, 3) (1, 4)")
print("coordinates of third row:         (2, 1) (2, 2) (2, 3)")
  
def get_hare_position(move):
    hare = np.where(move == 2)
    row = hare[0][0]
    col = hare[1][0]
    return (row, col)

def get_hound_positions(move):
    hounds=np.where(move == 1)
    row_hound_1 = hounds[0][0]
    col_hound_1 = hounds[1][0]
    row_hound_2 = hounds[0][1]
    col_hound_2 = hounds[1][1]
    row_hound_3 = hounds[0][2]
    col_hound_3 = hounds[1][2]
    return((row_hound_1, col_hound_1), (row_hound_2, col_hound_2), (row_hound_3, col_hound_3))

def is_legal_move(player, row_from, col_from, row_to, col_to):
    illegal_moves = [(0, 0), (2, 0), (0, 4), (2, 4)]
    moves_not_permitted = [[(0, 2), (1, 1)], [(0, 2), (1, 3)], [(1, 1), (2, 2)], [(1, 3), (2, 2)]]
    row_diff = abs(row_from - row_to)
    col_diff = abs(col_from - col_to)
    if player == 'hounds':
        if (row_to >= 0 and row_to < 3 and col_to >= 0 and col_to < 5):
            if move[row_to][col_to] == 0 and (row_to, col_to) not in illegal_moves and row_diff <= 1 and col_diff <= 1:
                if (col_to - col_from) < 0: # no moves to the left of the board
                    return False
                for item in moves_not_permitted:
                    if len(set([(row_from, col_from), (row_to, col_to)]).intersection(set(item))) == 2:
                        return False
                    else:
                        pass
                return True
        else:
            return False
    else: 
        if (row_to >= 0 and row_to < 3 and col_to >= 0 and col_to < 5):
            if move[row_to][col_to] == 0 and (row_to, col_to) not in illegal_moves and row_diff <= 1 and col_diff <= 1:
                for item in moves_not_permitted:
                    if len(set([(row_from, col_from), (row_to, col_to)]).intersection(set(item))) == 2:
                        return False
                    else:
                        pass
                return True

        else:
            return False

def possible_moves_list(row, col):
    top = (row - 1, col)
    bot = (row + 1, col)
    left = (row, col - 1)
    right = (row, col + 1)
    diagonal_top_left = (row - 1, col - 1)
    diagonal_top_right = (row - 1, col + 1)
    diagonal_bot_left = (row + 1, col - 1)
    diagonal_bot_right = (row + 1, col + 1)
    return [top, bot, left, right, diagonal_top_left, diagonal_top_right, diagonal_bot_left, diagonal_bot_right]
    
def get_next_moves(move, player):
    if player == 'hare':
        moves = []
        next_moves = []
        (row_from, col_from) = get_hare_position(move)
        moves = possible_moves_list(row_from, col_from)
        for mov in moves:
            row_to = mov[0]
            col_to = mov[1]
            if is_legal_move(player, row_from, col_from, row_to, col_to):
                next_moves.append(mov)
        return next_moves
        
    else:
        moves = []
        next_moves_hound1 = []
        next_moves_hound2 = []
        next_moves_hound3 = []
        (row_hound_1, col_hound_1),(row_hound_2, col_hound_2),(row_hound_3, col_hound_3) = get_hound_positions(move)
        moves_hound1 = possible_moves_list(row_hound_1, col_hound_1)
        moves_hound2 = possible_moves_list(row_hound_2, col_hound_2)
        moves_hound3 = possible_moves_list(row_hound_3, col_hound_3)
        for mov in moves_hound1:
            row_to = mov[0]
            col_to = mov[1]
            if is_legal_move(player, row_hound_1, col_hound_1, row_to, col_to):
                next_moves_hound1.append(mov)
        for mov in moves_hound2:
            row_to = mov[0]
            col_to = mov[1]
            if is_legal_move(player, row_hound_2, col_hound_2, row_to,col_to):
                 next_moves_hound2.append(mov)
        for mov in moves_hound3:
            row_to = mov[0]
            col_to = mov[1]
            if is_legal_move(player, row_hound_3, col_hound_3, row_to,col_to):
                next_moves_hound3.append(mov)
        return (next_moves_hound1, next_moves_hound2, next_moves_hound3)

def static_evaluation(move, player):
     start = np.array((1,4))
     goal = np.array((1,0))
     (row_hound_1, col_hound_1), (row_hound_2, col_hound_2), (row_hound_3,col_hound_3) = get_hound_positions(move)
     (row_hare, col_hare) = get_hare_position(move)
     hare = np.array((row_hare, col_hare))  
     hound1 = np.array((row_hound_1, col_hound_1))
     hound2 = np.array((row_hound_2, col_hound_2))
     hound3 = np.array((row_hound_3, col_hound_3))
     dist_hare_hound1 = cityblock(hare, hound1)
     dist_hare_hound2 = cityblock(hare, hound2)
     dist_hare_hound3 = cityblock(hare, hound3)
     dist_hare_goal = cityblock(hare, goal)
     dist_hare_start = cityblock(hare, start)
     dist_hound1_goal = cityblock(hound1, goal)
     dist_hound2_goal = cityblock(hound2, goal)
     dist_hound3_goal = cityblock(hound3, goal)
     dist_hound1_start = cityblock(hound1, start)
     dist_hound2_start = cityblock(hound2, start)
     dist_hound3_start = cityblock(hound1, start)
     if player == 'hare':
        score = 0
        if (col_hare < col_hound_3 and col_hare < col_hound_2 and col_hare < col_hound_1):
            print(col_hare, col_hound_1, col_hound_2, col_hound_3)
            print(move)
            score = math.pow(10, 5)
        else:
            score = dist_hare_goal - dist_hare_start
            if col_hare > col_hound_1:
                score += dist_hare_hound1 - 1 
            if col_hare > col_hound_2:
                score += dist_hare_hound2 - 1
            if col_hare > col_hound_3:
                score += dist_hare_hound3 - 1
            if col_hare == col_hound_1:
                score += dist_hare_hound1
            if col_hare == col_hound_2:
                score += dist_hare_hound2
            if col_hare == col_hound_3:
                score += dist_hare_hound3
        return score
     else:
        score = 0
        if (dist_hare_hound1 == 1 and dist_hare_hound2 == 1 and dist_hare_hound3 == 1):
            score = math.pow(10, 5)
        else:
            score = (dist_hare_hound3 + dist_hare_hound2 + dist_hare_hound1) / 3
            if col_hare > col_hound_1: 
                score += dist_hare_goal - dist_hare_hound1 - 1
            if col_hare > col_hound_2: 
                score += dist_hare_goal - dist_hare_hound2 - 1
            if col_hare > col_hound_3: 
                score += dist_hare_goal - dist_hare_hound3 - 1
            if col_hare == col_hound_1:
                score += dist_hare_hound1
            if col_hare == col_hound_2: 
                score += dist_hare_hound2
            if col_hare == col_hound_3:
                score += dist_hare_hound3
        return score

def alternate_player(player):
    if player == 'hare':
        alt_player = 'hounds'
    else:
        alt_player = 'hare'
    return alt_player

def generate_children(move, moves, player, row_hound=None, col_hound=None) :
    children = []
    for mov in moves :
        copy_board = move.copy()

        if player == 'hare':
            find_hare = np.where(move == 2)
            copy_board[find_hare] = 0
            if copy_board[mov[0]][mov[1]] == 0:
                copy_board[mov[0]][mov[1]] = 2
                children.append(copy_board)

        else:
            copy_board[row_hound][col_hound] = 0
            if copy_board[mov[0]][mov[1]] == 0:
                copy_board[mov[0]][mov[1]] = 1
                children.append(copy_board)
    return children

def alphabeta(move, depth, alpha, beta, player, maximizing_player):
    if depth == 0:
        return (move, static_evaluation(move, player))
    if maximizing_player:
        best_move = None
        children = []
        if player == 'hare':
            next_moves = get_next_moves(move, player)
            if next_moves:
                children_hare = generate_children(move, next_moves, player)
                if children_hare:
                    children = children_hare
        else:
            next_moves_hound1, next_moves_hound2, next_moves_hound3 = get_next_moves(move, player)
            (row_hound_1, col_hound_1), (row_hound_2, col_hound_2), (row_hound_3, col_hound_3) = get_hound_positions(move)
            if next_moves_hound1:
                children_hound1 = generate_children(move, next_moves_hound1, player, row_hound_1, col_hound_1)
                if children_hound1:
                    children += children + children_hound1
            if next_moves_hound2:
                children_hound2 = generate_children(move, next_moves_hound2, player, row_hound_2, col_hound_2)
                if children_hound2:
                    children += children + children_hound2
            if next_moves_hound3:
                children_hound3 = generate_children(move, next_moves_hound3, player, row_hound_3, col_hound_3)
                if children_hound3:
                    children += children + children_hound3
        for child in children:
            score = alphabeta(child, depth - 1, alpha, beta, alternate_player(player), False)[1]
            if score >alpha:
                best_move = child
                alpha = score
            if beta <= alpha:
                break
        return (best_move, alpha)
    else:
        best_move = None
        children = []
        if player == 'hare':
            next_moves = get_next_moves(move, player)
            if next_moves:
                children_hare = generate_children(move, next_moves, player)
                if children_hare:
                    children = children_hare
        else:
            next_moves_hound1, next_moves_hound2, next_moves_hound3 = get_next_moves(move, player)
            (row_hound_1, col_hound_1), (row_hound_2, col_hound_2), (row_hound_3, col_hound_3) = get_hound_positions(move)
            if next_moves_hound1:
                children_hound1 = generate_children(move, next_moves_hound1, player, row_hound_1, col_hound_1)
                if children_hound1:
                    children += children + children_hound1
            if next_moves_hound2:
                children_hound2 = generate_children(move, next_moves_hound2, player, row_hound_2, col_hound_2)
                if children_hound2:
                    children += children + children_hound2
            if next_moves_hound3:
                children_hound3 = generate_children(move, next_moves_hound3, player, row_hound_3, col_hound_3)
                if children_hound3:
                    children += children + children_hound3
        for child in children:
            score = alphabeta(child, depth - 1, alpha, beta, alternate_player(player), True)[1]
            if score < beta:
                best_move = child
                beta = score
            if beta <= alpha:
                break
        return (best_move, beta)
        
def game_ended(move,player):
    if player == 'hare' :
        return get_hare_position(move) == (0,1) or get_hare_position(move) == (1,0) 

gameover = False
human = True

while not gameover:
    if human:
        if player == 'hare':
            row_to = int(input("Enter row> "))
            col_to = int(input("Enter col> "))

            if move[row_to][col_to] == 0:
                find_hare = np.where(move == 2)
                row_from = find_hare[0][0]
                col_from = find_hare[1][0]

                if is_legal_move(player,row_from,col_from,row_to,col_to):
                    move[find_hare]= 0
                    move[row_to][col_to] = 2
                    print_game_state(move)
                else:
                    print_invalid()
            else:
                print_invalid()

        else:
            row_from = int(input('Enter row for hound you want to move> '))
            col_from = int(input('Enter col for hound you want to move> '))

            if move[row_from][col_from] == 1:
                """check if hound in that position and set to 0"""
                row_to = int(input('Enter row where you want to place hound> '))
                col_to = int(input('Enter col where you want to place hound> '))

                if is_legal_move(player,row_from,col_from,row_to,col_to):
                    move[row_from][col_from] = 0
                    move[row_to][col_to] = 1
                    print_game_state(move)
                else:
                    print_invalid()
            else:
                print_invalid()
        human = False
    else:
        computer_player = alternate_player(player)
        depth = depth
        alpha = -100000
        beta = 100000
        maximizing_player = True
        best_move, score = alphabeta(move, depth, alpha, beta, computer_player, maximizing_player)
        move = best_move
        print_game_state(move)
        if game_ended(move, player) :
            print("Congratulations!! " + player + " wins")
            gameover = True
        if game_ended(move, computer_player):
            print("Congratulations!! " + computer_player + " wins")
            gameover = True
        human = True

print("---GAME OVER---")
            

                

     
     
     
     
     
     
     
     



                        

